package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/IBM/sarama"
	_ "github.com/go-sql-driver/mysql"

	"ledger_service/internal/ledger"
)

const (
	dbDriver   = "mysql"
	dbUser     = "ledger_user"
	dbPassword = "Auth123"
	dbName     = "ledger"
	topic      = "ledger"
)

var (
	wg sync.WaitGroup
	db *sql.DB
)

type LedgerMsg struct {
	OrderID   string `json:"order_id"`
	UserID    string `json:"user_id"`
	Amount    int64  `json:"amount"`
	Operation string `json:"operation"`
	Date      string `json:"date"`
}

func main() {
	var err error

	// Open a database connection
	dsn := fmt.Sprintf("%s:%s@tcp(mysql-ledger:3306)/%s", dbUser, dbPassword, dbName)
	db, err := sql.Open(dbDriver, dsn)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		if err := db.Close(); err != nil {
			log.Printf("Error closing db: %s", err)
		}
	}()

	if err := db.Ping(); err != nil {
		log.Fatalf("Error pinging to the db: %v", err)
	}
	log.Println("pinged successfully")

	done := make(chan struct{})
	sarama.Logger = log.New(os.Stdout, "[sarama]", log.LstdFlags)

	consumer, err := sarama.NewConsumer([]string{"my-cluster-kafka-bootstrap:9092"}, sarama.NewConfig())
	if err != nil {
		log.Fatal("error on sarama.NewConsumer", err)
	}

	defer func() {
		close(done)
		if err := consumer.Close(); err != nil {
			log.Println("gracefully close channel and kafka consumer", err)
		}
	}()

	log.Println("sarama.NewConsumer created successfully")

	partitions, err := consumer.Partitions(topic)
	if err != nil {
		log.Fatal("consumer.Partitions", err)
	}
	for i := range partitions {
		partitionConsumer, err := consumer.ConsumePartition(topic, partitions[i], sarama.OffsetNewest)
		if err != nil {
			log.Fatal("consumer.ConsumePartition", err)
		}

		wg.Add(1)
		go func(pc sarama.PartitionConsumer, partition int32) {
			defer func() {
				if err := pc.Close(); err != nil {
					log.Println("gracefully close kafka partitionConsumer", err)
				}
				wg.Done()
			}()

			awaitMessages(partitionConsumer, partitions[i], done)
		}(partitionConsumer, partitions[i])
	}
	wg.Wait()
}

func awaitMessages(partitionConsumer sarama.PartitionConsumer, partition int32, done chan struct{}) {
	defer wg.Done()
	for {
		select {
		case msg, ok := <-partitionConsumer.Messages():
			if !ok {
				log.Println("Channel closed, exiting")
				return
			}

			log.Printf("Partition %d - Received message: %v\n", partition, msg)
			handleMessage(msg)
		case <-done:
			fmt.Printf("Received done signal. Exiting ...\n")
			return
		}
	}
}

func handleMessage(msg *sarama.ConsumerMessage) {
	var ledgerMessage LedgerMsg

	if err := json.Unmarshal(msg.Value, &ledgerMessage); err != nil {
		log.Printf("Error unmarshaling JSON: %v\n", err)
		return
	}

	if err := ledger.Insert(db, ledgerMessage.OrderID, ledgerMessage.UserID, ledgerMessage.Operation,
		ledgerMessage.Date, ledgerMessage.Amount); err != nil {
		log.Printf("Error ledger.Insert: %v\n", err)
		return
	}
}
