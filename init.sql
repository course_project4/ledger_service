create user 'ledger_user'@'%' identified by 'Auth123';

drop database  if exists ledger;
create database ledger;

GRANT ALL PRIVILEGES ON ledger.* TO 'ledger_user'@'%';

USE ledger;

create table if not exists `ledger` (
    id int not null auto_increment primary key,
    order_id varchar(255) not null unique,
    user_id varchar(255) not null,
    amount int not null,
    operation varchar(255) not null,
    transaction_date varchar(255) not null
);