package ledger

import "database/sql"

func Insert(db *sql.DB, orderID, userID, operation, date string, amount int64) error {
	stmt, err := db.Prepare("insert into ledger (order_id, user_id, amount, operation, transaction_date) values(?,?,?,?,?)")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(orderID, userID, amount, operation, date)
	if err != nil {
		return err
	}
	return nil
}
